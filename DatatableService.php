<?php

namespace LoicPennamen\SharedRepository;

use LoicPennamen\SharedRepository\DtColumn;
use Symfony\Component\HttpFoundation\Request;
use Twig\Environment;

class DatatableService
{
	private $twig;
	
	public function __construct(Environment $twig)
	{
		$this->twig = $twig;
	}
	
	/**
	 * @param array $columns
	 * @return array
	 */
	public function getDisabledColumnsAsArray(array $columns)
	{
		$arr = [];
		for ($i = 0; $i < sizeof($columns); $i++) {
			/** @var DtColumn $column */
			$column = $columns[$i];
			if(true !== $column->getSortable())
				$arr[] = $i;
		}
		return $arr;
	}
	
	/**
	 * Gets an array of opthions from request
	 *
	 * @param Request $request
	 * @return array
	 */
	public function getOptionsFromRequest(Request $request, array $tableColumns)
	{
		$options = array_merge([
			'andWhere' => [],
			'andWherePredicates' => [],
			'join' => [],
			'leftJoin' => [],
			'search' => [],
			"draw" => "1",
			"columns" => [],
			"start" => "0",
			"length" => "25",
			"tableColumns" => $tableColumns,
		], $options = $request->request->all());
		
		return $options;
	}
	
	/**
	 * @param $entity
	 * @param $columns
	 * @param null $context
	 * @return array
	 */
	public function getRow($entity, $columns, $context = null)
	{
		$row = [];
		
		/** @var DtColumn $column */
		foreach ($columns as $column)
			$row[] = $this->getCellValue($column, $entity, $context);
		
		return $row;
	}
	
	/**
	 * @param DtColumn $column
	 * @param $entity
	 * @param null $context
	 * @return string
	 */
	public function getCellValue(DtColumn $column, $entity, $context = null)
	{
		// Entity name, by convention
		$entityFullName = get_class($entity);
		$entityName = substr($entityFullName, strrpos($entityFullName, '\\') +1);
		$entitySlug = lcfirst($entityName);
		
		// Template
		$tplName = $entitySlug . '/cell-' . $column->getSlug() . '.html.twig';
		if(true === $this->twig->getLoader()->exists($tplName)){
			try {
				$cellHtml = $this->twig->render(
					$tplName,
					[$entitySlug => $entity, 'context' => $context]
				);
				return $cellHtml;
			} catch (\Exception $exception) {
				return $exception->getMessage();
			}
		}
		
		// Method name
		$methodName = 'get'.ucfirst($column->getSlug());
		if(method_exists($entity, $methodName)){
			try {
				return (string)$entity->{$methodName}();
			} catch (\Exception $exception) {
				return '['.$column->getSlug().']';
			}
		}
		
		return '[' . $column->getSlug() . ']';
	}
	
}
