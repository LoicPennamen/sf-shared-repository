<?php

namespace LoicPennamen\SharedRepository;

/**
 * Non-persisted entity designed for DataTable columns handling
 */
class DtColumn
{
	/**
	 * @var string
	 */
	private $slug;
	
	/**
	 * @var string
	 */
	private $name;
	
	/**
	 * @var string
	 */
	private $label;
	
	/**
	 * @var boolean
	 */
	private $sortable = true;
	
	/**
	 * @var string
	 */
	private $sortingKey = null;
	
	/**
	 * @var boolean
	 */
	private $htmlLabel = false;
	
	/**
	 * @var boolean
	 */
	private $sortNullValuesLast = true;
	
	
	
	
	public function setSlug($value){
		$this->slug = $value;
	}
	public function getSlug(){
		return $this->slug;
	}
	public function setName($value){
		$this->name = $value;
	}
	public function getName(){
		return $this->name;
	}
	public function setLabel($value){
		$this->label = $value;
	}
	public function getLabel(){
		return $this->label;
	}
	public function setSortable($value){
		$this->sortable = $value;
	}
	public function getSortable(){
		return $this->sortable;
	}
	public function setSortingKey($value){
		$this->sortingKey = $value;
	}
	public function getSortingKey(){
		return $this->sortingKey;
	}
	public function setHtmlLabel($value){
		$this->htmlLabel = $value;
	}
	public function getHtmlLabel(){
		return $this->htmlLabel;
	}
	public function setSortNullValuesLast($value){
		$this->sortNullValuesLast = $value;
	}
	public function getSortNullValuesLast(){
		return $this->sortNullValuesLast;
	}
}
