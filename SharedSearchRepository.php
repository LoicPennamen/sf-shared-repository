<?php

namespace LoicPennamen\SharedRepository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\QueryBuilder;

class SharedSearchRepository extends ServiceEntityRepository
{
	private $entityAlias;
	private $joins = [];
	private $leftJoins = [];
	private $searchFields = [];
	private $andWhere = [];
	private $orderBy = [];

	public function __construct(ManagerRegistry $registry, string $entityClass = '')
	{
		parent::__construct($registry, $entityClass);

		if(!$this->entityAlias)
			throw new \Exception("No entityAlias was defined for SharedSearchRepository");
	}

	public function search(array $options)
	{
		$options['query_type'] = 'search';
		$qb = $this->getBigQueryBuilder($options);

		return new Paginator($qb);
	}

	// Count with filters
	public function countSearch(array $options = null)
	{
		$options['query_type'] = 'count';
		$qb = $this->getBigQueryBuilder($options);
		$result = $qb->getQuery()->getOneOrNullResult();

		if(!$result)	return 0;
		else			return intval($result[1]);
	}

	// Count without filters but with mandatory parameters
	public function countSearchTotal(array $options)
	{
		$qb = $this->createQueryBuilder($this->entityAlias)
			->select("COUNT(DISTINCT($this->entityAlias.id))")
		;
		$qb = $this->addMandatoryParameters($qb, $options);
		$result = $qb->getQuery()->getOneOrNullResult();

		if(!$result)	return 0;
		else			return intval($result[1]);
	}

	/** Create queryBuilder for several methods
	 * @return \Doctrine\ORM\QueryBuilder
	 */
	public function getBigQueryBuilder($options = null)
	{
		$qb = $this->createQueryBuilder($this->entityAlias);

		// Selection subquery
		if($options['query_type'] == 'search')
			$qb->select($this->entityAlias);

		if($options['query_type'] == 'count')
			$qb->select("COUNT(DISTINCT($this->entityAlias.id))");

		// Add joins
		foreach ($this->joins as $joinField => $joinAlias)
			$qb->join($joinField, $joinAlias);
		foreach ($this->leftJoins as $joinField => $joinAlias)
			$qb->leftJoin($joinField, $joinAlias);

		// Search
		if($options['search'] && $options['search']['value'] && count($this->searchFields) > 0){
			$tmp = [];
			$searchValue = $options['search']['value'];
			$searchValue .= "%"; // Search "starting with..."

			// Search in...
			foreach($this->searchFields as $field)
				$tmp[] = " $field LIKE :searchValue ";

			$qb->andWhere("(".implode(' OR ', $tmp).")")
				->setParameter('searchValue', $searchValue)
			;
		}

		// Add filters
		foreach ($this->andWhere as $andWhere)
			$qb->andWhere($andWhere);

		// Specific mandatory parameters
		$qb = $this->addMandatoryParameters($qb, $options);

		// Sorting:
		if('search' === $options['query_type']
			&& isset($options['order'])
			&& isset($options['tableColumns']))
		{
			/** @var DtColumn $dtColumn */
			foreach($options['order'] as $order){
				$sortingColumnIndex = intval($order['column']);
				$dtColumn = $options['tableColumns'][$sortingColumnIndex];

				if(true === $dtColumn->getSortable()){

					// Vars from options
					$sortingKey = $dtColumn->getSortingKey()
						?: 	$this->entityAlias.'.'.$dtColumn->getSlug();
					$sortingDir = $order['dir'];

					// Nulls as first when sorting ASC
					if(false === $dtColumn->getSortNullValuesLast())
						$qb->addOrderBy($sortingKey, $sortingDir);

					// Nulls always last:
					else{
						// Pas de "order by case" en DQL. Pour passer les valeurs NULL à la fin,
						// on utilise un alias avec select HIDDEN et on ajoute un tri dessus
						$sortingAlias = "_sorting_alias_$sortingColumnIndex";
						$qb->addSelect(" CASE WHEN $sortingKey IS NULL THEN 1 ELSE 0 END AS HIDDEN $sortingAlias ");
						$qb->addOrderBy($sortingAlias);
						$qb->addOrderBy($sortingKey, $sortingDir);
					}
				}
			}
		}

		// Default sortings
		foreach($this->orderBy as $sortArr)
			$qb->addOrderBy($sortArr[0], $sortArr[1]);

		// Add parameters
		if('search' === $options['query_type']){
			$qb->setFirstResult($options['start']);
			$qb->setMaxResults($options['length']);
		}

		return $qb;
	}

	private function addMandatoryParameters(QueryBuilder $qb, array $options)
	{
		// Specific mandatory parameters
		foreach($options['andWhere'] as $field => $value){
			$valueAlias = str_replace('.', '_', $field);
			$qb->andWhere("$this->entityAlias.$field = :$valueAlias")
				->setParameter($valueAlias, $value);
		}

		return $qb;
	}

	public function setEntityAlias($entityAlias)
	{
		$this->entityAlias = $entityAlias;
	}

	public function addJoin($joinField, $joinAlias)
	{
		$this->joins[$joinField] = $joinAlias;
	}

	public function addLeftJoin($joinField, $joinAlias)
	{
		$this->leftJoins[$joinField] = $joinAlias;
	}

	public function addSearchField($searchField)
	{
		$this->searchFields[] = $searchField;
	}

	public function addAndWhere($andWhere)
	{
		$this->andWhere[] = $andWhere;
	}

	public function addOrderBy($orderBy, $dir = 'ASC')
	{
		$this->orderBy[] = [$orderBy, $dir];
	}
}
